import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.DefaultListModel;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Slider;

import com.ibm.icu.util.StringTokenizer;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.wb.swt.SWTResourceManager;


public class main {

	protected Shell shlJavaMplayer;
	private Text options;
	private Text Cache;
	private Text txtNewVideo;
	private Text txtdevvideo;
	private Text txtdevdsp;
	private Text txtNewUrl;
	private Text RadiotrayURL;
	
	public String dir = System.getProperty("user.home");
	public String dir2 = System.getProperty("user.home");
	public String dir3 = System.getProperty("user.home");
	public String dir4 = System.getProperty("user.home");

	private DefaultListModel list_MPlayer = new DefaultListModel();
	
	private Process mplayer_exec;
	private BufferedWriter mplayer_ecrire_flux;
	private BufferedReader mplayer_lire_flux;

	private int mplayer_etat=0;
	private int nb_fois_egal=0;
	private boolean repeter_playlist=false;
	private boolean plein_ecran=false;
	private boolean activer_video=true;
	private boolean activer_audio=true;
	private boolean curseur_activate=true;
	private boolean affichage_temps_mode=true;
	
	private int secondes_precedent=0;
	private int minutes_precedent=0;
	private int heures_precedent=0;
	private Label Temps;
	private List list;
	private Display display;
	private Scale CT;
	private Combo CDDevice;
	private Label Etat;
	private Button btnVcd;
	private Button btnCdrom;
	private Button btnDvd;
	private Combo Title1;
	private Combo Title2;
	private Button btnAudioDev;
	private Combo Norm;
	private Combo Driver;
	private Combo combo;
	private Combo ProgramURL;
	private Button btnRepeat;
	private Button btnFullscreen;
	private Combo format;
	private ProgressBar download;
	private Button btnGetFormatsAvailable;
	private Button btnVideo;
	private Button btnAudio;
	private Text txtCDDevice;
	private Button btnDevicewithHwinfo;
	private Button btnDevicewriteYour;
	private Button btnDetect;
	private Text txtUrlWith;
	private Button btnSelectAPlaylist;
	private Button btnLoadAJavamplayer;
	private Button btnSavePlaylist;
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			main window = new main();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlJavaMplayer.open();
		shlJavaMplayer.layout();
		while (!shlJavaMplayer.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void lecture_medias()
	{
	Etat.setText("Java MPlayer - Playing media");
	options.setText(list_MPlayer.getElementAt(list.getSelectionIndex()).toString());
	
	if(mplayer_etat==2)
		{
		try {
			mplayer_ecrire_flux.write("pause ");
			mplayer_ecrire_flux.newLine();
			mplayer_ecrire_flux.flush();
			mplayer_etat=1;
			} 
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			}
		}
	else 
		{
		CT.setSelection(0);
		if(mplayer_etat==1)
			{
			try {
				mplayer_ecrire_flux.write("stop");
				mplayer_ecrire_flux.newLine();
				mplayer_ecrire_flux.flush();
				mplayer_etat=0;
			} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			}
		}
		
		String nom_media_choisi=list.getItem(list.getSelectionIndex());
		String mplayer_media_choisi=list_MPlayer.getElementAt(list.getSelectionIndex()).toString();
			
		if(nom_media_choisi.startsWith("Web Media"))
			{
			curseur_activate=false;
			if(!mplayer_media_choisi.startsWith("http"))
				{
				try {
					Process programme_web = Runtime.getRuntime().exec(mplayer_media_choisi);
					BufferedReader programme_web_lire_flux = new BufferedReader(new InputStreamReader(programme_web.getInputStream()),1024*512);
					programme_web.waitFor();
					String fileurl = "";
					fileurl = programme_web_lire_flux.readLine();
					mplayer_media_choisi =  '"' + fileurl + '"';
					}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					} 
				catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					}
				}
			}
		else if(nom_media_choisi.startsWith("TV Media"))
			{
			curseur_activate=false;
			}
		else
			{
			curseur_activate=true;
			}
		
		CT.setEnabled(curseur_activate);
		
		ArrayList mplayer_commande = new ArrayList();
		mplayer_commande.add("/opt/javamplayer/bin/mplayer");
		mplayer_commande.add("-slave");
		mplayer_commande.add("-quiet");
		mplayer_commande.add("-subfont-osd-scale");
		mplayer_commande.add("2");
		mplayer_commande.add("-vo");
		if(activer_video==true)
			mplayer_commande.add("sdl");
		else
			mplayer_commande.add("null");
		
		if(activer_audio==false)
			{
			mplayer_commande.add("-ao");
			mplayer_commande.add("null");
			}
		
		if(plein_ecran==true)
			mplayer_commande.add("-fs");
		
		StringTokenizer MyBuffer = new StringTokenizer(mplayer_media_choisi, " ", false);
		File test_media = new File(mplayer_media_choisi);
		if (!test_media.exists())
			while(MyBuffer.hasMoreTokens())
				mplayer_commande.add(MyBuffer.nextToken());
		else
			mplayer_commande.add(mplayer_media_choisi);
		
		String[] commande_shell = new String[mplayer_commande.size()];
		for(int i=0;i<mplayer_commande.size();i++)
			commande_shell[i]=mplayer_commande.get(i).toString();
			
		try {
			Process pause_exec;
			pause_exec = Runtime.getRuntime().exec("sleep 1");
			pause_exec.waitFor();
			mplayer_exec = Runtime.getRuntime().exec(commande_shell);
			mplayer_ecrire_flux = new BufferedWriter(new OutputStreamWriter(mplayer_exec.getOutputStream()),1024*512);
			mplayer_lire_flux = new BufferedReader(new InputStreamReader(mplayer_exec.getInputStream()),1024*512);
				
			String waitcache = mplayer_lire_flux.readLine();
			boolean arret = false;
			while(!arret)
				{
				waitcache = mplayer_lire_flux.readLine();
				if (waitcache == null)
					{
					Etat.setText("Java MPlayer - Can't play media");
					mplayer_etat=-1;
					arret=true;
					CT.setEnabled(false);
					CT.setSelection(0);
					Temps.setText("00:00:00");
					}
				else if (waitcache.contains("Starting"))
					{
					mplayer_etat=1;
					arret=true;
					}
				}
			
			if(mplayer_etat==1)
				{
			String media_temps_max = "";
			while(!media_temps_max.startsWith("ANS"))
				{
				mplayer_ecrire_flux.write("get_time_length ");
				mplayer_ecrire_flux.newLine();
				mplayer_ecrire_flux.flush();
				media_temps_max = mplayer_lire_flux.readLine();
				}
			float temps_max = Float.valueOf(media_temps_max.split("=")[1]);
			CT.setMaximum((int) temps_max);
			
			
				}
			
			/*mplayer_ecrire_flux.write("get_meta_title");
			mplayer_ecrire_flux.newLine();
			mplayer_ecrire_flux.flush();
			String media_titre = "";
			while(!media_titre.startsWith("ANS"))
				media_titre = mplayer_lire_flux.readLine();
			media_titre = media_titre.split("=")[1];
			media_titre = media_titre.replaceAll("'", "");

			mplayer_ecrire_flux.write("get_meta_artist");
			mplayer_ecrire_flux.newLine();
			mplayer_ecrire_flux.flush();
			String media_artiste = "";
			while(!media_artiste.startsWith("ANS"))
				media_artiste = mplayer_lire_flux.readLine();
			media_artiste = media_artiste.split("=")[1];
			media_artiste = media_artiste.replaceAll("'", "");*/

	 	   	//Textes_Media.setText("<html>" + playlist.getSelectedValue().toString() + "<br>" + media_titre + "<br>" + media_artiste + "</html>");	 	   	
			} 
		catch (IOException e1) {
			// TODO Auto-generated catch block
			} 
		catch (InterruptedException e1) {
			// TODO Auto-generated catch block
		}
	}
}
	
	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlJavaMplayer = new Shell(SWT.SHELL_TRIM & (~SWT.RESIZE));
		shlJavaMplayer.setImage(SWTResourceManager.getImage(main.class, "/Images/icon.png"));
		shlJavaMplayer.addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(ShellEvent e) {
				if(mplayer_etat!=0)
					mplayer_exec.destroy();
				
			try {
					File fichier_playlist = new File(System.getProperty("user.home") + "/.javamplayer/playlist");
					fichier_playlist.createNewFile();
					FileWriter fstream = new FileWriter(System.getProperty("user.home") + "/.javamplayer/playlist");
					BufferedWriter ecrire_fichier = new BufferedWriter(fstream);
					for (int ligne=0; ligne < list.getItemCount(); ligne++)
						{
						ecrire_fichier.write(list.getItem(ligne) + "\n");
						ecrire_fichier.write(list_MPlayer.get(ligne).toString() + "\n");
						}
					ecrire_fichier.close();
					fstream.close();		
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.exit(1);	
			}
		});
		shlJavaMplayer.setSize(748, 527);
		shlJavaMplayer.setText("Java MPlayer");
		shlJavaMplayer.setLayout(null);
		
		Etat = new Label(shlJavaMplayer, SWT.NONE);
		Etat.setFont(SWTResourceManager.getFont("Droid Sans", 10, SWT.BOLD));
		Etat.setAlignment(SWT.CENTER);
		Etat.setBounds(10, 10, 360, 17);
		Etat.setText("Java MPlayer - Stopped");
		
		Temps = new Label(shlJavaMplayer, SWT.NONE);
		Temps.setBounds(10, 33, 360, 17);
		Temps.setText("00:00:00");
		
		
		Button btnNewButton = new Button(shlJavaMplayer, SWT.NONE);
		btnNewButton.setFont(SWTResourceManager.getFont("Droid Sans", 8, SWT.NORMAL));
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(list.getSelectionIndex()!=-1)
    				{
					if(list.getSelectionIndex()==0)
						list.setSelection(list.getItemCount()-1);					
					else
						list.setSelection(list.getSelectionIndex()-1);	
					mplayer_etat=1;
					lecture_medias();
    			}
			}
		});
		btnNewButton.setBounds(10, 72, 40, 27);
		btnNewButton.setText("<<");
		
		Button button = new Button(shlJavaMplayer, SWT.NONE);
		button.setFont(SWTResourceManager.getFont("Droid Sans", 8, SWT.NORMAL));
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
        		if(list.getSelectionIndex()!=-1)
    				{
        			if(list.getSelectionIndex()==list.getItemCount()-1)
        				list.setSelection(0);					
        			else
        				list.setSelection(list.getSelectionIndex()+1);		
        			mplayer_etat=1;
        			lecture_medias();
    				}
			}
		});
		button.setBounds(330, 72, 40, 27);
		button.setText(">>");
		
		Button btnPlay = new Button(shlJavaMplayer, SWT.NONE);
		btnPlay.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(list.getSelectionIndex()!=-1)
        			lecture_medias();
			}
		});
		btnPlay.setBounds(142, 72, 96, 27);
		btnPlay.setText("Play");
		
		list = new List(shlJavaMplayer, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
		list.setFont(SWTResourceManager.getFont("Droid Sans", 8, SWT.NORMAL));
		list.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				if(list.getSelectionIndex()!=-1)
        			options.setText(list_MPlayer.getElementAt(list.getSelectionIndex()).toString());
        		else
        			options.setText("");
			}
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(list.getSelectionIndex()!=-1)
        			options.setText(list_MPlayer.getElementAt(list.getSelectionIndex()).toString());
        		else
        			options.setText("");
			}
		});
		list.setBounds(10, 105, 360, 221);
		
		Button btnStop = new Button(shlJavaMplayer, SWT.NONE);
		btnStop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					if(mplayer_etat!=0)
						{
						Etat.setText("Java MPlayer - Stopped");
						Temps.setText("00:00:00");
						CT.setSelection(0);
						CT.setEnabled(false);
						mplayer_ecrire_flux.write("stop ");
						mplayer_ecrire_flux.newLine();
						mplayer_ecrire_flux.flush();
						mplayer_etat=0;
						}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnStop.setBounds(56, 72, 80, 27);
		btnStop.setText("Stop");
		
		Button btnPause = new Button(shlJavaMplayer, SWT.NONE);
		btnPause.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					if(mplayer_etat!=0)
						{
						mplayer_ecrire_flux.write("pause ");
						mplayer_ecrire_flux.newLine();
						mplayer_ecrire_flux.flush();
						if(mplayer_etat==1)
							{
							Etat.setText("Java MPlayer - Paused");
							mplayer_etat=2;
							}
						else
							{
							Etat.setText("Java MPlayer - Playing media");
							mplayer_etat=1;
							}
						}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnPause.setBounds(244, 72, 80, 27);
		btnPause.setText("Pause");
		
		Button btnRemove = new Button(shlJavaMplayer, SWT.NONE);
		btnRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int [] index = list.getSelectionIndices();
				
				int pos = 999999999;
				for(int i = index.length -1;i>=0;i--)
                	if (pos > index[i])
                		pos = index[i];
				
				for(int i = index.length -1;i>=0;i--)
                	{
					list.remove(index[i]);
					list_MPlayer.remove(index[i]);
                	}
				
				int size = list.getItemCount();
				
				if(pos==size)
					{
					list.setSelection(pos-1);
					}
	            else 
	            	list.setSelection(pos);
				
				if(size>0)
					options.setText(list_MPlayer.getElementAt(list.getSelectionIndex()).toString());
				else
					options.setText("");

			}
		});
		btnRemove.setBounds(95, 365, 178, 27);
		btnRemove.setText("Remove selected items");
		
		TabFolder tabFolder = new TabFolder(shlJavaMplayer, SWT.NONE);
		tabFolder.setBounds(376, 10, 358, 382);
		
		TabItem tbtmAddFiles = new TabItem(tabFolder, SWT.NONE);
		tbtmAddFiles.setText("Add Files");
		
		Composite composite = new Composite(tabFolder, SWT.NONE);
		tbtmAddFiles.setControl(composite);
		
		Label lblThisSectionPermits = new Label(composite, SWT.NONE);
		lblThisSectionPermits.setBounds(10, 10, 332, 68);
		lblThisSectionPermits.setText("This section permits you to add files on your computer.\n\nNote : Avoid to name your files with special characters\nor with space.");
		
		Button btnAddFiles = new Button(composite, SWT.NONE);
		btnAddFiles.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(shlJavaMplayer, SWT.MULTI);
		        fd.setText("Select your files");
		        fd.setFilterPath(dir);
		        String[] filterExt = { "*.*" };
		        fd.setFilterExtensions(filterExt);
		        String selected = fd.open();
		        if (selected != null)
		        	{
		        	String[] files = fd.getFileNames();
		        	String directory_of_files = fd.getFilterPath();
		        	int n=0;
		        	for(n=0;n<files.length;n++)
		        		{
		        		list.add(files[n]);		        	
		        		list_MPlayer.addElement(directory_of_files + "/" + files[n]);
		        		}
		        	}
		        	//lblGamename.setText(lblGamename.getText());
		        //else
		        	//{
		        	//lblGamename.setText(fd.getFileName());
		        	//game = selected;
		        	//}
		        dir=fd.getFilterPath();
			}
		});
		btnAddFiles.setBounds(10, 310, 332, 27);
		btnAddFiles.setText("Add file(s) on your playlist");
		
		TabItem tbtmAddDvdVcd = new TabItem(tabFolder, SWT.NONE);
		tbtmAddDvdVcd.setText("Add DVD, VCD or CD");
		
		Composite composite_1 = new Composite(tabFolder, SWT.NONE);
		tbtmAddDvdVcd.setControl(composite_1);
		
		btnDvd = new Button(composite_1, SWT.RADIO);
		btnDvd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(btnDvd.getSelection())
					{	
					CDDevice.setEnabled(true);
					Title1.setEnabled(false);
					Title2.setEnabled(false);
					Cache.setEnabled(false);
					}
			}
		});
		btnDvd.setSelection(true);
		btnDvd.setBounds(10, 10, 67, 21);
		btnDvd.setText("DVD");
		
		btnVcd = new Button(composite_1, SWT.RADIO);
		btnVcd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(btnVcd.getSelection())
				{	
				CDDevice.setEnabled(true);
				Title1.setEnabled(true);
				Title2.setEnabled(true);
				Cache.setEnabled(false);
				}
			}
		});
		btnVcd.setText("VCD");
		btnVcd.setBounds(275, 10, 67, 21);
		
		btnCdrom = new Button(composite_1, SWT.RADIO);
		btnCdrom.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(btnCdrom.getSelection())
				{	
				CDDevice.setEnabled(true);
				Title1.setEnabled(true);
				Title2.setEnabled(true);
				Cache.setEnabled(true);
				}
			}
		});
		btnCdrom.setText("CD-ROM");
		btnCdrom.setBounds(133, 10, 84, 21);
		
		Label lblNewLabel_1 = new Label(composite_1, SWT.NONE);
		lblNewLabel_1.setBounds(10, 235, 62, 17);
		lblNewLabel_1.setText("Add title :");
		
		Title1 = new Combo(composite_1, SWT.NONE);
		Title1.setEnabled(false);
		Title1.setItems(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"});
		Title1.setBounds(78, 230, 75, 27);
		Title1.select(0);
		
		Label lblTo = new Label(composite_1, SWT.NONE);
		lblTo.setText("to");
		lblTo.setBounds(169, 235, 15, 17);
		
		Title2 = new Combo(composite_1, SWT.NONE);
		Title2.setEnabled(false);
		Title2.setItems(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"});
		Title2.setBounds(200, 230, 67, 27);
		Title2.select(0);
		
		Label lblCacheSize = new Label(composite_1, SWT.NONE);
		lblCacheSize.setText("Cache :");
		lblCacheSize.setBounds(10, 268, 49, 17);
		
		Cache = new Text(composite_1, SWT.BORDER);
		Cache.setEnabled(false);
		Cache.setText("1024");
		Cache.setBounds(78, 263, 75, 27);
		
		CDDevice = new Combo(composite_1, SWT.NONE);
		CDDevice.setBounds(10, 70, 332, 27);
		
		Button btnAddYourMedia = new Button(composite_1, SWT.NONE);
		btnAddYourMedia.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String name_device = null;
				String selected_device = null;
				
				if(btnDevicewithHwinfo.getSelection()==true)
					{
					selected_device = CDDevice.getItem(CDDevice.getSelectionIndex());
					String [] device = selected_device.split(" = ");
					name_device = device[0];
					}
				else
					{
					selected_device = txtCDDevice.getText();
					name_device = txtCDDevice.getText();
					}
					
				if(btnDvd.getSelection())
					{	
					list.add("DVD Media : " + selected_device);
	        		list_MPlayer.addElement("dvdnav:// -nocache -mouse-movements -dvd-device " + name_device);
					}
				else if(btnVcd.getSelection())
					{	
					int begin = Title1.getSelectionIndex()+1;
					int end = Title2.getSelectionIndex()+1;
					for(int vcd=begin;vcd<=end;vcd++)
    					{
						list.add("VCD Media Title " + vcd +  " : " + selected_device);
						list_MPlayer.addElement("vcd://" + vcd + " -cdrom-device " + name_device);
    					}
					}
				else if(btnCdrom.getSelection())
					{	
					float nombre_pistes = 99;
	        		Process mplayer_exec;
					try {
						mplayer_exec = Runtime.getRuntime().exec("/opt/javamplayer/bin/mplayer -identify cdda:// -frames 0 -vc null -vo null -ao null -cdrom-device " + name_device);
						BufferedWriter mplayer_ecrire_flux = new BufferedWriter(new OutputStreamWriter(mplayer_exec.getOutputStream()),1024*512);
						BufferedReader mplayer_lire_flux = new BufferedReader(new InputStreamReader(mplayer_exec.getInputStream()),1024*512);
						
						String waitcache = mplayer_lire_flux.readLine();
						while(!waitcache.startsWith("ID_CDDA_TRACKS="))
							waitcache = mplayer_lire_flux.readLine();
						nombre_pistes = Float.valueOf(waitcache.split("=")[1]);

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					int begin = Title1.getSelectionIndex()+1;
					int end = Title2.getSelectionIndex()+1;
					for(int cd=begin;cd<=end;cd++)
    					{
						list.add("CD Media Title " + cd +  " : " + selected_device);
						list_MPlayer.addElement("cdda://" + cd + " -cdrom-device " + name_device + " -cache " + Cache.getText());
    					}
					}
			}
		});
		btnAddYourMedia.setText("Add your media");
		btnAddYourMedia.setBounds(10, 310, 332, 27);
		
		Label lblHwinfoMustBe = new Label(composite_1, SWT.NONE);
		lblHwinfoMustBe.setFont(SWTResourceManager.getFont("Droid Sans", 9, SWT.BOLD));
		lblHwinfoMustBe.setText("In this case, hwinfo must be installed on your PC.");
		lblHwinfoMustBe.setBounds(10, 103, 332, 17);
		
		Label lblSudoAptgetInstall = new Label(composite_1, SWT.NONE);
		lblSudoAptgetInstall.setFont(SWTResourceManager.getFont("Droid Sans", 9, SWT.ITALIC));
		lblSudoAptgetInstall.setText("sudo apt-get install hwinfo");
		lblSudoAptgetInstall.setBounds(10, 122, 332, 17);
		
		Label lblAndRestartJava = new Label(composite_1, SWT.NONE);
		lblAndRestartJava.setText("And restart Java MPlayer after the setup.");
		lblAndRestartJava.setFont(SWTResourceManager.getFont("Droid Sans", 9, SWT.NORMAL));
		lblAndRestartJava.setBounds(10, 142, 332, 17);
		
		btnDevicewithHwinfo = new Button(composite_1, SWT.CHECK);
		btnDevicewithHwinfo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(btnDevicewithHwinfo.getSelection()==true)
					{
					CDDevice.setEnabled(true);
					btnDevicewriteYour.setSelection(false);
					txtCDDevice.setEnabled(false);
					btnDetect.setEnabled(true);
					}
			}
		});
		btnDevicewithHwinfo.setSelection(true);
		btnDevicewithHwinfo.setBounds(10, 43, 152, 21);
		btnDevicewithHwinfo.setText("Device (with hwinfo)");
		
		btnDevicewriteYour = new Button(composite_1, SWT.CHECK);
		btnDevicewriteYour.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(btnDevicewriteYour.getSelection()==true)
					{
					CDDevice.setEnabled(false);
					btnDevicewithHwinfo.setSelection(false);
					txtCDDevice.setEnabled(true);
					btnDetect.setEnabled(false);
					}
			}	
		});
		btnDevicewriteYour.setText("Device (write your CD/DVD/VCD device here)");
		btnDevicewriteYour.setBounds(10, 165, 288, 21);
		
		btnDetect = new Button(composite_1, SWT.NONE);
		btnDetect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try
				{
				CDDevice.removeAll();
				Process find_drives = Runtime.getRuntime().exec("hwinfo --short --cdrom");
				BufferedReader lshw_lire_flux = new BufferedReader(new InputStreamReader(find_drives.getInputStream()),1024*512);
				
				String waitcache = lshw_lire_flux.readLine();
				waitcache = lshw_lire_flux.readLine();
				while(waitcache!=null)
					{
					StringBuilder texte = new StringBuilder();
					int n=0, t=0;
					for(n=2;n<waitcache.length();n++)
						{
						char letter = waitcache.charAt(n);
						if(letter!=' ')
							texte.append(letter);
						else
							n = waitcache.length();
						}
					texte.append(' ');
					texte.append('=');
					texte.append(' ');

					int end = 15;
					for(n=15;n<waitcache.length();n++)
						{
						char letter = waitcache.charAt(n);
						
						while (letter==' ')
							{
							n++;
							letter = waitcache.charAt(n);
							}
						
						end = n;
						n = waitcache.length();
						}
					
					for(n=end;n<waitcache.length();n++)
						{
						char letter = waitcache.charAt(n);
						texte.append(letter);
						}
					
					CDDevice.add(texte.toString());
					CDDevice.select(0);
					
					waitcache = lshw_lire_flux.readLine();
					
					}
				}
			catch (IOException ex)
				{
				
				} 
			}
		});
		btnDetect.setBounds(260, 37, 82, 27);
		btnDetect.setText("Detect");
		
		txtCDDevice = new Text(composite_1, SWT.BORDER);
		txtCDDevice.setEnabled(false);
		txtCDDevice.setText("/dev/dvd");
		txtCDDevice.setBounds(10, 188, 332, 27);
		
		TabItem tbtmAddTv = new TabItem(tabFolder, SWT.NONE);
		tbtmAddTv.setText("Add TV");
		
		Composite composite_4 = new Composite(tabFolder, SWT.NONE);
		tbtmAddTv.setControl(composite_4);
		
		txtNewVideo = new Text(composite_4, SWT.BORDER);
		txtNewVideo.setText("New Video");
		txtNewVideo.setBounds(86, 10, 256, 27);
		
		txtdevvideo = new Text(composite_4, SWT.BORDER);
		txtdevvideo.setText("/dev/video0");
		txtdevvideo.setBounds(86, 43, 256, 27);
		
		txtdevdsp = new Text(composite_4, SWT.BORDER);
		txtdevdsp.setEnabled(false);
		txtdevdsp.setText("/dev/dsp");
		txtdevdsp.setBounds(86, 76, 256, 27);
		
		Norm = new Combo(composite_4, SWT.NONE);
		Norm.setItems(new String[] {"NTSC", "PAL", "SECAM"});
		Norm.setBounds(86, 109, 256, 27);
		Norm.select(0);
		
		Driver = new Combo(composite_4, SWT.NONE);
		Driver.setItems(new String[] {"v4l", "v4l2"});
		Driver.setBounds(86, 142, 256, 27);
		Driver.select(0);
		
		combo = new Combo(composite_4, SWT.NONE);
		combo.setItems(new String[] {"11025", "22050", "44100", "48000"});
		combo.setBounds(86, 175, 256, 27);
		combo.select(0);
		
		Label lblName = new Label(composite_4, SWT.NONE);
		lblName.setBounds(10, 15, 62, 17);
		lblName.setText("Name :");
		
		Label lblVideoDev = new Label(composite_4, SWT.NONE);
		lblVideoDev.setText("Video dev :");
		lblVideoDev.setBounds(10, 48, 70, 17);
		
		Label lblNorm = new Label(composite_4, SWT.NONE);
		lblNorm.setText("Norm :");
		lblNorm.setBounds(10, 114, 70, 17);
		
		Label lblDriver = new Label(composite_4, SWT.NONE);
		lblDriver.setText("Driver :");
		lblDriver.setBounds(10, 147, 70, 17);
		
		Label lblFrequency = new Label(composite_4, SWT.NONE);
		lblFrequency.setText("Freq :");
		lblFrequency.setBounds(10, 180, 70, 17);
		
		Button button_1 = new Button(composite_4, SWT.NONE);
		button_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(txtdevdsp.isEnabled())
					{
					list.add("TV Media : " + txtNewVideo.getText() + "(V:" + txtdevvideo.getText() + " , A:" + txtdevdsp.getText() + ")");
	    			list_MPlayer.addElement("tv:// -tv device=" + txtdevvideo.getText() + ":norm=" + Norm.getItem(Norm.getSelectionIndex()) + ":driver=" + Driver.getItem(Driver.getSelectionIndex()) + ":adevice=" + txtdevdsp.getText() + ":audiorate=" + combo.getItem(combo.getSelectionIndex()) + ":forceaudio:immediatemode=0");
					}
				else
					{
					list.add("TV Media : " + txtNewVideo.getText() + "(V:" + txtdevvideo.getText() + ")");
					list_MPlayer.addElement("tv:// -tv device=" + txtdevvideo.getText() + ":norm=" + Norm.getItem(Norm.getSelectionIndex()) + ":driver=" + Driver.getItem(Driver.getSelectionIndex()));
					}
			}
		});
		button_1.setText("Add your media");
		button_1.setBounds(10, 310, 332, 27);
		
		btnAudioDev = new Button(composite_4, SWT.CHECK);
		btnAudioDev.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				txtdevdsp.setEnabled(btnAudioDev.getSelection());
			}
		});
		btnAudioDev.setBounds(10, 78, 70, 21);
		btnAudioDev.setText("Audio :");
		
		TabItem tbtmAddUrl = new TabItem(tabFolder, SWT.NONE);
		tbtmAddUrl.setText("Add URL");
		
		Composite composite_5 = new Composite(tabFolder, SWT.NONE);
		tbtmAddUrl.setControl(composite_5);
		
		txtNewUrl = new Text(composite_5, SWT.BORDER);
		txtNewUrl.setText("Your name without space");
		txtNewUrl.setBounds(86, 109, 256, 27);
		
		Label label = new Label(composite_5, SWT.NONE);
		label.setText("Name :");
		label.setBounds(10, 113, 62, 17);
		
		Label lblUrl = new Label(composite_5, SWT.NONE);
		lblUrl.setText("Playlist :");
		lblUrl.setBounds(10, 47, 70, 17);
		
		RadiotrayURL = new Text(composite_5, SWT.BORDER);
		RadiotrayURL.setEnabled(false);
		RadiotrayURL.setText("/usr/share/radiotray/bookmarks.xml");
		RadiotrayURL.setBounds(86, 43, 256, 27);
		
		Label lblProgram = new Label(composite_5, SWT.NONE);
		lblProgram.setText("Program :");
		lblProgram.setBounds(10, 14, 70, 17);
		
		ProgramURL = new Combo(composite_5, SWT.NONE);
		ProgramURL.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (ProgramURL.getSelectionIndex()==0)
					{
					RadiotrayURL.setEnabled(false);
					btnSelectAPlaylist.setEnabled(false);
					txtNewUrl.setEnabled(true);
					txtUrlWith.setEnabled(true);
					format.setEnabled(false);
					btnGetFormatsAvailable.setEnabled(false);
					}
				else if (ProgramURL.getSelectionIndex()==1)
					{
					RadiotrayURL.setEnabled(true);
					btnSelectAPlaylist.setEnabled(true);
					txtNewUrl.setEnabled(false);
					txtUrlWith.setEnabled(false);
					format.setEnabled(false);
					btnGetFormatsAvailable.setEnabled(false);
					}
				else if (ProgramURL.getSelectionIndex()==2)
					{
					RadiotrayURL.setEnabled(false);
					btnSelectAPlaylist.setEnabled(false);
					txtNewUrl.setEnabled(true);
					txtUrlWith.setEnabled(true);
					format.setEnabled(true);
					btnGetFormatsAvailable.setEnabled(true);
					}
			}
		});
		ProgramURL.setItems(new String[] {});
		ProgramURL.setBounds(86, 10, 256, 27);
		ProgramURL.select(0);
		
		Button button_2 = new Button(composite_5, SWT.NONE);
		button_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				download.setSelection(0);
        		if(ProgramURL.getSelectionIndex()==0)
    			{
    			list.add("Web Media - URL - : " + txtNewUrl.getText());
    			list_MPlayer.addElement(txtUrlWith.getText());
    			}
    		else if(ProgramURL.getSelectionIndex()==1)
    			{
    			try {
    				FileInputStream fstream = new FileInputStream(RadiotrayURL.getText());
    	        	DataInputStream in = new DataInputStream(fstream);
    	        	BufferedReader lire_ligne = new BufferedReader(new InputStreamReader(in));
            	
    	        	String strLine;
    	        	String[] contenu = null;
    	        	while(true) {
    	        		  strLine = lire_ligne.readLine(); 
    	        		  if (strLine==null)
    	        			  break;
    	        		  if (strLine.contains("bookmark name") && !strLine.contains("[separator-"))
    	        		  	  {
    	        			  strLine = strLine.replace("/>", "");
    	        			  strLine = strLine.replace('"' + "", "");
    	        			  contenu = strLine.split("name=")[1].split("url=");
    	        		  	  
    	        			  java.util.List<String> liste = Arrays.asList(contenu);
    	        		  	      	        			  
    	        		  	  list.add("Web Media - RadioTray - : " + liste.get(0));
    	        		  	  list_MPlayer.addElement(liste.get(1));
    	        		  	  }
    	        		  }
    	        	
    				lire_ligne.close();
    				in.close();
    				fstream.close();
    			} catch (FileNotFoundException e1) {
    				// TODO Auto-generated catch block
    				e1.printStackTrace();
    				} catch (IOException e1) {
    				// TODO Auto-generated catch block
    				e1.printStackTrace();
    				}
    			}
    		else if(ProgramURL.getSelectionIndex()==2)
    			{
    			boolean downloadetat=false;
    			Etat.setText("Java MPlayer - Adding media");
    			
				try {
					
					String valeur = format.getItem(format.getSelectionIndex());
					valeur = valeur.split("\t")[0];
					Process youtube_exec = Runtime.getRuntime().exec("/opt/javamplayer/bin/youtube-dl " + txtUrlWith.getText() + " -f " + valeur + " --newline -o " + System.getProperty("user.home") + "/.javamplayer/videos" + "/" + txtNewUrl.getText() + ".javamplayer");
					BufferedReader youtube_lire_flux = new BufferedReader(new InputStreamReader(youtube_exec.getInputStream()),1024*512);
					
	    			String waitcache = youtube_lire_flux.readLine();
	    			waitcache = youtube_lire_flux.readLine();
	    			
	    			while(waitcache!=null)
	    				{
	    				if(waitcache.contains("%"))
	    					{
	    					downloadetat = true;
	    					String pourcentage = waitcache.split("\t")[0];
	    					pourcentage = pourcentage.split("%")[0];
	    					pourcentage = pourcentage.replace("[download]", "");
	    					pourcentage = pourcentage.replaceAll(" ", "");
	    					float pos = Float.valueOf(pourcentage);
	    					download.setSelection((int) pos*10);
	    					}
	    				waitcache = youtube_lire_flux.readLine();
	    				}
	    			
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        		
				if(downloadetat==true)
					{
					list.add(txtNewUrl.getText() + ".javamplayer");
					list_MPlayer.addElement(System.getProperty("user.home") + "/.javamplayer/videos" + "/" + txtNewUrl.getText() + ".javamplayer");
					}
				else
					{
					Etat.setText("Java MPlayer - Can't add media");
					mplayer_etat=-1;
					}
				}
			}
		});
		button_2.setText("Add your media");
		button_2.setBounds(10, 310, 332, 27);
		
		options = new Text(shlJavaMplayer, SWT.BORDER);
		options.setFont(SWTResourceManager.getFont("Droid Sans", 8, SWT.NORMAL));
		options.setBounds(10, 431, 360, 27);
		
		Button btnModifyCommandFor = new Button(shlJavaMplayer, SWT.NONE);
		btnModifyCommandFor.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
    			list_MPlayer.set(list.getSelectionIndex(), options.getText());
			}
		});
		btnModifyCommandFor.setText("Modify command for the selected item");
		btnModifyCommandFor.setBounds(10, 464, 360, 27);
		
		CT = new Scale(shlJavaMplayer, SWT.NONE);
		CT.setEnabled(false);
		CT.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {				
    			int position = CT.getSelection();
    	        int pourcentage = (position * 100 / CT.getMaximum()) + 1;
    	        String valeur = String.valueOf(pourcentage);
    			try {
    				mplayer_ecrire_flux.write("seek " + valeur + " 1 ");
    				mplayer_ecrire_flux.newLine();
    				mplayer_ecrire_flux.flush();
    	        } catch (IOException ex) {
    	        }
    			
				affichage_temps_mode=true;
			}
			@Override
			public void mouseDown(MouseEvent e) {
				affichage_temps_mode=false;
			}
		});
		CT.setBounds(10, 51, 360, 19);

		File dossier_mere = new File(System.getProperty("user.home") + "/.javamplayer/");
        if(!dossier_mere.exists())
        	dossier_mere.mkdir();
        
        File dossier_videos = new File(System.getProperty("user.home") + "/.javamplayer/videos");
        if(!dossier_videos.exists())
        	dossier_videos.mkdir();
        
        File scriptdir = new File(System.getProperty("user.home") + "/.javamplayer/scripts");
        ProgramURL.add("None");
        ProgramURL.add("Radiotray");
        ProgramURL.add("Youtube (Youtube-dl)");
        ProgramURL.select(0);
        
        Label lblRadiotray = new Label(composite_5, SWT.NONE);
        lblRadiotray.setText("RadioTray : http://doc.ubuntu-fr.org/radiotray\nYoutube-dl : http://rg3.github.io/youtube-dl/");
        lblRadiotray.setBounds(10, 270, 332, 34);
        
        Label lblFormat = new Label(composite_5, SWT.NONE);
        lblFormat.setText("Format :");
        lblFormat.setBounds(10, 174, 70, 17);
        
        format = new Combo(composite_5, SWT.NONE);
        format.setEnabled(false);
        format.setItems(new String[] {});
        format.setBounds(86, 170, 256, 27);
        format.select(0);
        
        btnGetFormatsAvailable = new Button(composite_5, SWT.NONE);
        btnGetFormatsAvailable.setEnabled(false);
        btnGetFormatsAvailable.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		format.removeAll();
        		Process youtube_exec;
        		BufferedReader youtube_lire_flux;
        		
        		try {
					youtube_exec = Runtime.getRuntime().exec("/opt/javamplayer/bin/youtube-dl -F -q " + txtUrlWith.getText());
	        		youtube_lire_flux = new BufferedReader(new InputStreamReader(youtube_exec.getInputStream()),1024*512);

	    			String waitcache = youtube_lire_flux.readLine();
	    			waitcache = youtube_lire_flux.readLine();
	    			while(waitcache!=null)
	    				{
	    				format.add(waitcache);
	    				//waitcache = waitcache.replaceAll("\t", "");
	    				//waitcache = waitcache.replaceAll(":", " : ");
	    				//System.out.println(waitcache);
	    				waitcache = youtube_lire_flux.readLine();  				
	    				}
	    			format.select(0);

        		} catch (IOException e1) {
					// TODO Auto-generated catch block
				}
        	}
        });
        btnGetFormatsAvailable.setText("Get available formats for this video (Youtube-dl)");
        btnGetFormatsAvailable.setBounds(10, 203, 332, 27);
        
        download = new ProgressBar(composite_5, SWT.NONE);
        download.setMaximum(1000);
        download.setBounds(10, 254, 332, 10);
        
        Label lblDowloadProgression = new Label(composite_5, SWT.NONE);
        lblDowloadProgression.setText("Download progression (Youtube-dl) :");
        lblDowloadProgression.setBounds(10, 236, 332, 17);
        
        btnSelectAPlaylist = new Button(composite_5, SWT.NONE);
        btnSelectAPlaylist.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		FileDialog fd = new FileDialog(shlJavaMplayer);
		        fd.setText("Select your playlist file of RadioTray");
		        fd.setFilterPath(dir2);
		        String[] filterExt = { "*.*" };
		        fd.setFilterExtensions(filterExt);
		        String selected = fd.open();
		        if (selected != null)
		        	{
		        	String file = fd.getFileName();
		        	String directory_of_files = fd.getFilterPath();
		        	dir2 = directory_of_files;
		        	RadiotrayURL.setText(directory_of_files + "/" + file);
		        	}
        	}
        });
        btnSelectAPlaylist.setText("Select a playlist file (RadioTray)");
        btnSelectAPlaylist.setEnabled(false);
        btnSelectAPlaylist.setBounds(10, 76, 332, 27);
        
        txtUrlWith = new Text(composite_5, SWT.BORDER);
        txtUrlWith.setBounds(86, 137, 256, 27);
        
        Label lblUrl_1 = new Label(composite_5, SWT.NONE);
        lblUrl_1.setText("URL :");
        lblUrl_1.setBounds(10, 143, 62, 17);
        
        Canvas logo = new Canvas(shlJavaMplayer, SWT.NONE);
        logo.setBackgroundImage(SWTResourceManager.getImage(main.class, "/Images/Logo.png"));
        logo.setBounds(376, 398, 358, 93);
        
        btnRepeat = new Button(shlJavaMplayer, SWT.NONE);
        btnRepeat.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        btnRepeat.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		if(repeter_playlist==false)
        			{
        			btnRepeat.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
        			repeter_playlist=true;
        			}
        		else
        			{
        			btnRepeat.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        			repeter_playlist=false;
        			}
        	}
        });
        btnRepeat.setText("Repeat");
        btnRepeat.setBounds(279, 398, 91, 27);
        
        btnFullscreen = new Button(shlJavaMplayer, SWT.NONE);
        btnFullscreen.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		if(plein_ecran==false)
    				{
        			btnFullscreen.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
        			plein_ecran=true;
    				}
        		else
    				{
        			btnFullscreen.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        			plein_ecran=false;
    				}
        	}
        });
        btnFullscreen.setText("Fullscreen");
        btnFullscreen.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        btnFullscreen.setBounds(10, 398, 80, 27);
        
        btnVideo = new Button(shlJavaMplayer, SWT.NONE);
        btnVideo.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		if(activer_video==false)
    				{
        			btnVideo.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
        			activer_video=true;
    				}
        		else
    				{
        			btnVideo.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        			activer_video=false;
    				}
        	}
        });
        btnVideo.setText("Video");
        btnVideo.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
        btnVideo.setBounds(95, 398, 94, 27);
        
        Button btnMoveUp = new Button(shlJavaMplayer, SWT.NONE);
        btnMoveUp.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		if(list.getSelectionIndex()>0)
				{
				String nom1=list.getItem(list.getSelectionIndex());
				String cmd1=list_MPlayer.get(list.getSelectionIndex()).toString();
				String nom2=list.getItem(list.getSelectionIndex()-1);
				String cmd2=list_MPlayer.get(list.getSelectionIndex()-1).toString();
			
				list.setItem(list.getSelectionIndex(), nom2);
				list.setItem(list.getSelectionIndex()-1, nom1);
				list_MPlayer.set(list.getSelectionIndex(), cmd2);
				list_MPlayer.set(list.getSelectionIndex()-1, cmd1);
				
				list.setSelection(list.getSelectionIndex()-1);
				}
        	}
        });
        btnMoveUp.setText("Move Up");
        btnMoveUp.setBounds(10, 365, 79, 27);
        
        Button btnMoveDown = new Button(shlJavaMplayer, SWT.NONE);
        btnMoveDown.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
				if(list.getSelectionIndex()<list.getItemCount()-1)
				{
				String nom1=list.getItem(list.getSelectionIndex());
				String cmd1=list_MPlayer.get(list.getSelectionIndex()).toString();
				String nom2=list.getItem(list.getSelectionIndex()+1);
				String cmd2=list_MPlayer.get(list.getSelectionIndex()+1).toString();
				
				list.setItem(list.getSelectionIndex(), nom2);
				list.setItem(list.getSelectionIndex()+1, nom1);
				list_MPlayer.set(list.getSelectionIndex(), cmd2);
				list_MPlayer.set(list.getSelectionIndex()+1, cmd1);
				
				list.setSelection(list.getSelectionIndex()+1);
				}
        	}
        });
        btnMoveDown.setText("Move Down");
        btnMoveDown.setBounds(279, 365, 91, 27);
        
        btnAudio = new Button(shlJavaMplayer, SWT.NONE);
        btnAudio.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		if(activer_audio==false)
    				{
        			btnAudio.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
        			activer_audio=true;
    				}
        		else
    				{
        			btnAudio.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
        			activer_audio=false;
    				}
        	}
        });
        btnAudio.setText("Audio");
        btnAudio.setForeground(SWTResourceManager.getColor(SWT.COLOR_DARK_GREEN));
        btnAudio.setBounds(193, 398, 80, 27);
        
        btnSavePlaylist = new Button(shlJavaMplayer, SWT.NONE);
        btnSavePlaylist.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		FileDialog fd = new FileDialog(shlJavaMplayer, SWT.SAVE);
		        fd.setText("Save playlist");
		        fd.setFilterPath(dir3);
		        fd.setFilterNames(new String[] {"Java MPlayer playlist files (*.jmplaylist)"});
		        fd.setFilterExtensions(new String[] {"*.jmplaylist"});
		        String filename = fd.open();
		        if (filename != null)
		        	{		        	
		        	try {
						File fichier_playlist = new File(filename);
						fichier_playlist.createNewFile();
						FileWriter fstream = new FileWriter(filename);
						BufferedWriter ecrire_fichier = new BufferedWriter(fstream);
						for (int ligne=0; ligne < list.getItemCount(); ligne++)
							{
							ecrire_fichier.write(list.getItem(ligne) + "\n");
							ecrire_fichier.write(list_MPlayer.get(ligne).toString() + "\n");
							}
						ecrire_fichier.close();
						fstream.close();		
		        	} catch (IOException e1) {
		        		// TODO Auto-generated catch block
		        		e1.printStackTrace();
		        	}
		        	
		        	}

        	}
        });
        btnSavePlaylist.setText("Save playlist");
        btnSavePlaylist.setBounds(10, 332, 104, 27);
        
        btnLoadAJavamplayer = new Button(shlJavaMplayer, SWT.NONE);
        btnLoadAJavamplayer.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		FileDialog fd = new FileDialog(shlJavaMplayer, SWT.OPEN);
		        fd.setText("Open a playlist");
		        fd.setFilterPath(dir4);
		        fd.setFilterNames(new String[] {"Java MPlayer playlist files (*.jmplaylist)"});
		        fd.setFilterExtensions(new String[] {"*.jmplaylist"});
		        String filename = fd.open();
		        if (filename != null)
		        	{
		        	File fichier_playlist = new File(filename);
		        	if(fichier_playlist.exists())
                		{
		        		try {
		        			FileInputStream fstream = new FileInputStream(filename);
		        			DataInputStream in = new DataInputStream(fstream);
		        			BufferedReader lire_ligne = new BufferedReader(new InputStreamReader(in));
		        			String strLine;
		        			while(true) {
        	        		  // Print the content on the console
        	        		  strLine = lire_ligne.readLine();  
        	        		  if (strLine==null)
        	        			  break;
        	        		  list.add(strLine);
        	        		  strLine = lire_ligne.readLine();
        	        		  if (strLine==null)
        	        			  break;
        	        		  list_MPlayer.addElement(strLine);
        	        		  }
		        			lire_ligne.close();
		        			in.close();
		        			fstream.close();
        				} catch (FileNotFoundException e1) {
        					// TODO Auto-generated catch block
        					e1.printStackTrace();
        				} catch (IOException e1) {
        					// TODO Auto-generated catch block
        					e1.printStackTrace();
        					}
                		}
		        	}
        	}
        });
        btnLoadAJavamplayer.setText("Open a playlist");
        btnLoadAJavamplayer.setBounds(252, 332, 118, 27);
        
        Button btnRemoveAllItems = new Button(shlJavaMplayer, SWT.NONE);
        btnRemoveAllItems.addSelectionListener(new SelectionAdapter() {
        	@Override
        	public void widgetSelected(SelectionEvent e) {
        		int index = list.getItemCount();
								
				for(int i = index-1;i>=0;i--)
                	{
					list.remove(i);
					list_MPlayer.remove(i);
                	}
				
				options.setText("");
        	}
        });
        btnRemoveAllItems.setText("Remove all items");
        btnRemoveAllItems.setBounds(120, 332, 126, 27);
        final Color wColor = new Color(display, new RGB(0x10, 0x10, 0x10));
        
        File fichier_playlist = new File(System.getProperty("user.home") + "/.javamplayer/playlist");
        if(fichier_playlist.exists())
        	{
			try {
				FileInputStream fstream = new FileInputStream(System.getProperty("user.home") + "/.javamplayer/playlist");
	        	DataInputStream in = new DataInputStream(fstream);
	        	BufferedReader lire_ligne = new BufferedReader(new InputStreamReader(in));
	        	String strLine;
	        	while(true) {
	        		  // Print the content on the console
	        		  strLine = lire_ligne.readLine();  
	        		  if (strLine==null)
	        			  break;
	        		  list.add(strLine);
	        		  strLine = lire_ligne.readLine();
	        		  if (strLine==null)
	        			  break;
	        		  list_MPlayer.addElement(strLine);
	        		  }
				lire_ligne.close();
				in.close();
				fstream.close();
				} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
        	}
        
		Runnable timer = new Runnable() {
		      public void run() {
		      int time=0;
		      
		      if(affichage_temps_mode==true)
		      	{
					try 
					{
					time=1000;
					
					if(mplayer_etat!=-1)
						{
					String media_temps_max = "";
					while(!media_temps_max.startsWith("ANS_LENGTH="))
						{
						mplayer_ecrire_flux.write("get_time_length ");
						mplayer_ecrire_flux.newLine();
						mplayer_ecrire_flux.flush();
						media_temps_max = mplayer_lire_flux.readLine();
						}
					float temps_max = Float.valueOf(media_temps_max.split("=")[1]);
					CT.setMaximum((int) temps_max);
					
			        String media_position_temps = "0";
			        while(!media_position_temps.startsWith("ANS_TIME_POSITION="))
			        	{
						mplayer_ecrire_flux.write("get_time_pos ");
						mplayer_ecrire_flux.newLine();
						mplayer_ecrire_flux.flush();
			        	media_position_temps = mplayer_lire_flux.readLine();
			        	}
			        	
			        media_position_temps = media_position_temps.split("=")[1];
			        float Temps_Position = Float.valueOf(media_position_temps);
			        if (curseur_activate==true) 
			        	CT.setSelection((int) Temps_Position);
			        
			        int secondes;
			        int minutes=(int) Temps_Position/60;
			        int heures = minutes/60;
			        
			        if (heures==0)
			            {
			            if (minutes==0)
			            	secondes=(int) Temps_Position;
			            else
			            	secondes=(int) Temps_Position-minutes*60;
			            }
			        else
			            {
			        	minutes=(int) Temps_Position/60-heures*60;
			        	secondes=(int) Temps_Position-(heures*3600+minutes*60);
			            }

			    	String texte_heures, texte_minutes, texte_secondes;
			        
			        if(heures<10)
			        	texte_heures = ("0"+String.valueOf(heures));
			        else
			        	texte_heures = (String.valueOf(heures));

			        if(minutes<10)
			        	texte_minutes = ("0"+String.valueOf(minutes));
			        else
			        	texte_minutes = (String.valueOf(minutes));

			        if(secondes<10)
			        	texte_secondes = ("0"+String.valueOf(secondes));
			        else
			        	texte_secondes = (String.valueOf(secondes));
			        			        
			        Temps.setText(texte_heures + ":" + texte_minutes + ":" + texte_secondes);
			        
			        if(secondes == secondes_precedent && minutes == minutes_precedent && heures == heures_precedent && nb_fois_egal < 2)
			        	{
			        	nb_fois_egal=nb_fois_egal+1;
			        	}
			        else if(secondes == secondes_precedent && minutes == minutes_precedent && heures == heures_precedent && nb_fois_egal >= 2)
		        		{
			        	Etat.setText("Java MPlayer - Paused");
			        	mplayer_etat=2;
		        		}	        	
			        else
			        	{
			        	nb_fois_egal=0;
			        	Etat.setText("Java MPlayer - Playing media");
			        	mplayer_etat=1;
			        	}
			        secondes_precedent = secondes;
			        minutes_precedent = minutes;
			        heures_precedent = heures;
						}
					}	
				
				catch (Exception ev) {
			       	if(list.getSelectionIndex()!=-1)
	        			{
			       		if(list.getSelectionIndex()==list.getItemCount()-1 && repeter_playlist==true && mplayer_etat!=0)
			       			{
			       			list.setSelection(0);
			       			lecture_medias();
			       			}
			       		else if(list.getSelectionIndex()<list.getItemCount()-1 && mplayer_etat!=0)
			       			{
			       			list.setSelection(list.getSelectionIndex()+1);					
			       			lecture_medias();
			       			}
			       		else
			       			{
			       			Etat.setText("Java MPlayer - Stopped");
			       			mplayer_etat=0;
			       			CT.setSelection(0);
			       			CT.setEnabled(false);
			       			}
	        			}
					}
				}
		      else
		      	{
		    	  time=10;
		    	  int Temps_Position=CT.getSelection();
		    	  int secondes;
		    	  int minutes=(int) Temps_Position/60;
		    	  int heures = minutes/60;
	        
		    	  if (heures==0)
  					{
		    		  if (minutes==0)
		    			  secondes=(int) Temps_Position;
		    		  else
		    			  secondes=(int) Temps_Position-minutes*60;
  					}
		    	  else
	            	{
		    		  minutes=(int) Temps_Position/60-heures*60;
		    		  secondes=(int) Temps_Position-(heures*3600+minutes*60);
	            	}

		    	  String texte_heures, texte_minutes, texte_secondes;
	        
		    	  if(heures<10)
		    		  texte_heures = ("0"+String.valueOf(heures));
		    	  else
		    		  texte_heures = (String.valueOf(heures));

		    	  if(minutes<10)
		    		  texte_minutes = ("0"+String.valueOf(minutes));
		    	  else
		    		  texte_minutes = (String.valueOf(minutes));

		    	  if(secondes<10)
		    		  texte_secondes = ("0"+String.valueOf(secondes));
		    	  else
		    		  texte_secondes = (String.valueOf(secondes));
  			
		    	  Temps.setText(texte_heures + ":" + texte_minutes + ":" + texte_secondes);
		      	}
		        display.timerExec(time, this);
		      }
		    };
		    display.timerExec(1000, timer);
		    
	}
}
