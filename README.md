Java MPlayer est une interface graphique Java pour Linux qui vous permettra d'utiliser une version de MPlayer que j'ai modifié.

Celle-ci améliore la lecture des DVD (menus avec les icones d'origine, sélection des sous-titres et de la langue via le menu du DVD, …).

Voir le fichier SETUP.txt pour installer le logiciel.

Email : jaime.gemelo@hotmail.fr
Site Web : http://jaime.gemelo.free.fr