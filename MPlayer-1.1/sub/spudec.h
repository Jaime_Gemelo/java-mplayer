#ifndef MPLAYER_SPUDEC_H
#define MPLAYER_SPUDEC_H

#include "libvo/video_out.h"

void spudec_heartbeat(void *this, unsigned int pts100);
void spudec_assemble(void *this, unsigned char *packet, unsigned int len, int pts100);
void spudec_draw(void *this, void (*draw_alpha)(int x0,int y0, int w,int h, int dp, unsigned char* src, unsigned char *srca, int stride));
void spudec_draw_scaled(void *this, unsigned int dxs, unsigned int dys, void (*draw_alpha)(int x0,int y0, int w,int h, int dp, unsigned char* src, unsigned char *srca, int stride));
void spudec_update_palette(void *this, unsigned int *palette);
void *spudec_new_scaled(unsigned int *palette, unsigned int frame_width, unsigned int frame_height, uint8_t *extradata, int extradata_len);
void *spudec_new(unsigned int *palette);
void spudec_free(void *this);
void spudec_reset(void *this);	// called after seek
int spudec_visible(void *this); // check if spu is visible
void spudec_set_font_factor(void * this, double factor); // sets the equivalent to ffactor
void spudec_set_hw_spu(void *this, const vo_functions_t *hw_spu);
int spudec_changed(void *this);
void spudec_calc_bbox(void *me, unsigned int dxs, unsigned int dys, unsigned int* bbox);
void spudec_set_forced_subs_only(void * const this, const unsigned int flag);

// Convert yuv color to rgb color
void spu_yuv_to_rgb(unsigned int y,unsigned int u,unsigned int v,
    unsigned int *r,unsigned int *g,unsigned int *b);
// Enable/disable dvdmenu mode, and set color mode
//	cflg = 0:	Y SPU (default)
//	cflg = 1:	YUV SPU
//	cflg = 2:	RGB SPU
//	cflg = 3:	BGR SPU
//	cflg = 4:	YUY SPU
void spudec_dvdnav_mode(void *this, int mode, int cflg);
// Set dvd menu button draw area and palette
void spudec_dvdnav_area(void *this, uint16_t sx, uint16_t sy,
    uint16_t ex, uint16_t ey, uint32_t palette);
// Set dvd menu button palette
void spudec_dvdnav_palette(void *this, uint32_t palette);
// Draw scaled image in YUV and YUY mode
void spudec_draw_scaled_yuv(void *me, unsigned int dxs, unsigned int dys,
    void (*draw_alpha)(int x0,int y0, int w,int h, int dp, unsigned char* src, unsigned char *srca, int stride));
// Draw scaled image in RGB and BGR mode
void spudec_draw_scaled_rgb(void *me, unsigned int dxs, unsigned int dys,
    void (*draw_alpha)(int x0,int y0, int w,int h, int dp, unsigned char* src, unsigned char *srca, int stride));
// Convert Yuv image to YuY image
void spudec_create_yuy(void *this, int spu_scaled);

#endif /* MPLAYER_SPUDEC_H */
